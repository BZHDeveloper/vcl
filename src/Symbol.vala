namespace Vcl {
	public interface Symbol : CodeNode {
		public abstract string name { owned get; }
		
		public abstract Symbol? parent_symbol { owned get; }
		
		public abstract string full_name { owned get; }
	}
}
