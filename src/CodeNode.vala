namespace Vcl {
	public interface CodeNode : Gee.Hashable<CodeNode> {
		public abstract string to_string();
		
		public abstract CodeNode? parent_node { owned get; }
		
		public abstract SourceReference source_reference { owned get; }
	}
}
