namespace Vcl {
	public class SourceReference : Gee.Hashable<SourceReference>, GLib.Object {
		public Source source { get; construct; }
		
		public SourceLocation begin { get; construct; }
		
		public SourceLocation end { get; construct; }
		
		public string to_string() {
			return "%s:%s:%s".printf (source.file.get_path(), begin.to_string(), end.to_string());
		}
		
		public bool equal_to (SourceReference reference) {
			return str_equal (to_string(), reference.to_string());
		}
		
		public uint hash() {
			return str_hash (to_string());
		}
	}
}
