namespace Vcl {
	public abstract class AbstractSymbol : AbstractCodeNode, Symbol {
		public abstract string name { owned get; }
		
		public abstract Symbol? parent_symbol { owned get; }
		
		public virtual string full_name {
			owned get {
				if (parent_symbol is Symbol)
					return parent_symbol.full_name + "." + name;
				return name;
			}
		}
		
		
	}
}
