namespace Vcl {
	public class File : AbstractSource {
		Gee.ArrayList<Symbol> symbol_list;
		
		construct {
			symbol_list = new Gee.ArrayList<Symbol>();
		}
		
		internal File (GLib.File file) {
			GLib.Object (file : file);
		}
		
		public override GLib.File file { get; construct; }
		
		public override Gee.List<Symbol> symbols {
			get {
				return symbol_list;
			}
		}
		
		public static File open (GLib.File file) throws GLib.IOError {
			if (!file.query_exists())
				throw new IOError.NOT_FOUND ("file not found");
			return new File (file);
		}
		
		public static File open_path (string path) throws GLib.IOError {
			return open (GLib.File.new_for_path (path));
		}
		
		public static File open_uri (string uri) throws GLib.IOError {
			return open (GLib.File.new_for_uri (uri));
		}
	}
}
