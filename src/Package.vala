namespace Vcl {
	public interface Package : Source {
		public abstract string id { get; construct; }
		
		public abstract string name { get; construct; }
		
		public abstract string description { get; construct; }
	}
}
