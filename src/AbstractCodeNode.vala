namespace Vcl {
	public abstract class AbstractCodeNode : Gee.Hashable<CodeNode>, CodeNode, GLib.Object {
		public abstract string to_string();
		
		public virtual bool equal_to (CodeNode code_node) {
			return str_equal (to_string(), code_node.to_string());
		}
		
		public virtual uint hash() {
			return str_hash (to_string());
		}
		
		public abstract CodeNode? parent_node { owned get; }
		
		public abstract SourceReference source_reference { owned get; }
	}
}
