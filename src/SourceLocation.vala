namespace Vcl {
	public class SourceLocation : Gee.Hashable<SourceLocation>, GLib.Object {
		public uint line { get; construct; }
		
		public uint column { get; construct; }
		
		public string to_string() {
			return "{%u-%u}".printf (line, column);
		}
		
		public bool equal_to (SourceLocation location) {
			return str_equal (to_string(), location.to_string());
		}
		
		public uint hash() {
			return str_hash (to_string());
		}
		
		public bool before (SourceLocation location) {
			return line < location.line || line == location.line && column < location.column;
		}
		
		public bool inside (SourceReference reference) {
			return reference.begin.before (this) && before (reference.end);
		}
	}
}
