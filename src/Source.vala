namespace Vcl {
	public interface Source : Gee.Hashable<Source> {
		public abstract GLib.File file { get; construct; }
		
		public abstract Gee.List<Symbol> symbols { get; }
	}
}
