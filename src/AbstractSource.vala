namespace Vcl {
	public abstract class AbstractSource : Gee.Hashable<Source>, Source, GLib.Object {
		public abstract GLib.File file { get; construct; }
		
		public abstract Gee.List<Symbol> symbols { get; }
		
		public virtual bool equal_to (Source source) {
			return file.equal (source.file);
		}
		
		public virtual uint hash() {
			return file.hash();
		}
	}
}
